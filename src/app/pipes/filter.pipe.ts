import {
  Pipe,
  PipeTransform
} from '@angular/core';

@Pipe({
  name: 'cfilter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any[], filterString: string, propName: string): any[] {
    debugger;
    const resultArray = [];
    if (value.length === 0 || filterString === '' || propName === '') {
      return value;
    }

    for (const item of value) {

      let x = item[propName];
      if (x)
        x = x.toString();
      if (x && x.toLocaleLowerCase().includes(filterString.toLocaleLowerCase())) {
        resultArray.push(item);
      }
    }
    return resultArray;
  }

}

// .toLocaleLowerCase().includes("filterString"))
// === filterString
