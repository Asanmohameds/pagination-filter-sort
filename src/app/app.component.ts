import {
  Component
} from '@angular/core';

import {
  IcontractNo,
  IemployeeId,
  IfourfieldsType,
  Iticket,
  IcategoryType,
  IsubCategoryType
} from './propertyInterface';

import {
  TicketsDataApiService
} from './tickets-data-api.service';

import {
  FormBuilder,
  Validators
} from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  //********        Property data types declaration     ************/

  ticketList: Iticket[] = [];
  contractNumberList: IcontractNo[] = [];
  employeeIdList: IemployeeId[] = [];
  ticketTypeIdList: IfourfieldsType[] = [];
  departmentIdList: IfourfieldsType[] = [];
  assignedToList: IfourfieldsType[] = [];
  priorityList: IfourfieldsType[] = [];
  categoryList: IcategoryType[] = [];
  subCategoryList: IsubCategoryType[] = [];

  // contractNumber: any;
  filterContractNumber = '';
  searchContractNumber = '';
  sortbyParam = '';
  sortDirection = 'asc';
  pageStartFrom: number = 1;
  defineItemsPerPage:number = 5;


  constructor(private fb: FormBuilder, private ticketApiData: TicketsDataApiService) {}


  ngOnInit(): void {

    this.getEntireTicketList();

  }

  definerRecordCount() {
    this.pageStartFrom = 1;
  }

  isPopOpen = false;
  isLoading = false;

  closePop() {
    this.isPopOpen = false;
  }

  //Forms declaration:
  ticketForm = this.fb.group({
    contractNumber: ['', Validators.required],
    employeeId: ['', Validators.required],
    ticketType: ['', Validators.required],
    departmentSection: ['', Validators.required],
    assignedTo: ['', Validators.required],
    priorityType: ['', Validators.required],
    categoryId: ['', Validators.required],
    subCategory: ['', Validators.required],
    ticketSubject: ['', Validators.required],
    description: ['', Validators.required]
  });



  //**************Processing with URL to take Data***************/

  getEntireTicketList() {
    debugger;
    this.isLoading=true;
    this.ticketApiData.urlErp().subscribe(data => {
      //debugger;
      this.isLoading=false;
      this.ticketList = data.Data;
      //console.log(this.ticketList);

    });
  }

  openCreate() {
    debugger;
    
    this.isPopOpen = true;
    this.isLoading=true;
    if (!this.contractNumberList || this.contractNumberList.length < 1)
    
      this.getContractUrlList()

    if (!this.ticketTypeIdList || this.ticketTypeIdList.length < 1)
      this.getTicketTypeUrlList()
    
    this.isLoading=false;
  }

  getContractUrlList() {
    //debugger;
    this.isLoading=true;
    this.ticketApiData.urlContract().subscribe(data => {
      this.isLoading=false;
      this.contractNumberList = data.Result2;
      //console.log(this.contractNumberList);
    });
  }

  getEmployeeUrlList() {
    //debugger;
    this.isLoading=true;
    let contractnumber = this.ticketForm.value.contractNumber;
    this.ticketApiData.urlEmployee(contractnumber).subscribe(data => {
      this.isLoading=false;
      //debugger
      this.employeeIdList = data;
      // console.log(this.employeeIdList);
    });
  }

  getTicketTypeUrlList() {
    //debugger;
    this.isLoading=true;
    this.ticketApiData.urlTicketType().subscribe(data => {
      this.isLoading=false;
      this.ticketTypeIdList = data.Data;
    });
  }

  getDepartmentUrlList(event: any) {
    //debugger;
    this.isLoading=true;
    let _ticketId = this.ticketForm.value.ticketType;
    this.ticketApiData.urlDepartmentId(_ticketId).subscribe(data => {
      this.isLoading=false;
      this.departmentIdList = data;
      debugger;
      //console.log(this.departmentIdList);

      // These below lines not in scope just to take a value of selected box

      //***       In three ways we can take selected box value      ******/
    //let text1  = event.target.options[event.target.options.selectedIndex].innerText;

    //***    this is second way , this way is best ********/
    let text = event.target.selectedOptions[0].innerText;


    //***  this is third way    *****/

    // let opt = this.ticketTypeIdList.filter(x => {
    //       return x.ID== event.target.value    
    //   });
    //  let text2 = opt[0].Name
    // alert("text2 - " + text2 )

    });
  }

  getAssignedToUrlList() {
    this.isLoading=true;
    this.ticketApiData.urlAssignedTo().subscribe(data => {
      this.isLoading=false;
      this.assignedToList = data.Data;
      //console.log(this.assignedToList);
    });
  }

  getPriority() {
    this.isLoading=true;
    this.ticketApiData.urlPriorityList().subscribe(data => {
      this.isLoading=false;
      this.priorityList = data.Data;
    });
  }

  getCategoryList() {
    this.isLoading=true;
    let _depId = this.ticketForm.value.departmentSection
    this.ticketApiData.urlCategoryList(_depId).subscribe(data => {
      this.isLoading=false;
      this.categoryList = data;
      console.log("this.categoryList");
    });
  }

  getSubCategoryList() {
    this.isLoading=true;
    let _categoryId = this.ticketForm.value.categoryId
    this.ticketApiData.urlSubcategoryList(_categoryId).subscribe(data => {
      this.isLoading=false;
      this.subCategoryList = data;
    });
  }

  submitData() {

    //debugger;
    if (this.ticketForm.valid) {

      let formval: any = this.ticketForm.value;

      let params = {
        cusnam: "",
        custid: "CIN0000150",
        emil: "",
        descp: formval.description,
        contno: "",
        Page: "",
        PageSize: "",
        Priority: formval.priorityType,
        Group: formval.categoryId,
        SubGroup: formval.subCategory,
        Subject: formval.ticketSubject,
        AssignedTo: formval.assignedTo,
        TicketType: formval.ticketType,
        TicketAssignGroup: formval.departmentSection,
        ContractNumber: formval.contractNumber,
        LabourNumber: formval.employeeId,
        TicketChannel: 2,
        UserId: "cc.user",
        accuracy: "",
      };

      this.ticketApiData.postData(params).subscribe(data => {

        alert(data);
        this.getEntireTicketList();
        this.closePop();
      });
    }
    this.ticketForm.reset();
  }

  onContractNoFilter() {
    this.searchContractNumber = this.filterContractNumber;
  }

 filterCol = {
  col:"",
  value:""
  }

  filterText:any ={

    EnquiryId:null,
    CreatedDatetime:null,
    CreatedBy:null,
    ContractNumber:null,
    CustomerId:null,
    CustomerName:null,
    StatusName:null,
    TicketTypeId:null,
    PriorityName:null,
    Subject:null


  }

  SearchFilter(Column:any) {

    debugger;
    this.filterCol.col = Column;
    this.filterCol.value = this.filterText[Column] ; 

  }

  onContractNoclear() {
    this.searchContractNumber='';
    this.filterContractNumber='';
  }


  onSortDirecetion(field:any) {

    this.sortbyParam = field;
    if(this.sortDirection === 'desc') {
      this.sortDirection = 'asc';
    } else {
      this.sortDirection = 'desc';
    }
  }


}
